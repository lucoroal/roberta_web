UPDATE wp_options set option_value=REPLACE(option_value, 'www.sistiroberta.dev', 'www.sistiroberta.it')
WHERE `option_value` LIKE '%www.sistiroberta.dev%' ;

UPDATE wp_postmeta set meta_value=REPLACE(meta_value, 'www.sistiroberta.dev', 'www.sistiroberta.it')
WHERE `meta_value` LIKE '%www.sistiroberta.dev%' ;

UPDATE wp_posts set post_content=REPLACE(post_content, 'www.sistiroberta.dev', 'www.sistiroberta.it'),
guid=REPLACE(guid, 'www.sistiroberta.dev', 'www.sistiroberta.it')
WHERE `guid` LIKE '%www.sistiroberta.dev%' OR `post_content` LIKE '%www.sistiroberta.dev%' ;

UPDATE wp_usermeta set meta_value=REPLACE(meta_value, 'www.sistiroberta.dev', 'www.sistiroberta.it')
WHERE `meta_value` LIKE '%www.sistiroberta.dev%' ;

UPDATE wp_users set user_url=REPLACE(user_url, 'www.sistiroberta.dev', 'www.sistiroberta.it')
WHERE `user_url` LIKE '%www.sistiroberta.dev%' ;