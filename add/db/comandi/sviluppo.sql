UPDATE wp_options set option_value=REPLACE(option_value, 'www.sistiroberta.it', 'www.sistiroberta.dev')
WHERE `option_value` LIKE '%www.sistiroberta.it%' ;

UPDATE wp_postmeta set meta_value=REPLACE(meta_value, 'www.sistiroberta.it', 'www.sistiroberta.dev')
WHERE `meta_value` LIKE '%www.sistiroberta.it%' ;

UPDATE wp_posts set post_content=REPLACE(post_content, 'www.sistiroberta.it', 'www.sistiroberta.dev'),
guid=REPLACE(guid, 'www.sistiroberta.it', 'www.sistiroberta.dev')
WHERE `guid` LIKE '%www.sistiroberta.it%' OR `post_content` LIKE '%www.sistiroberta.it%' ;

UPDATE wp_usermeta set meta_value=REPLACE(meta_value, 'www.sistiroberta.it', 'www.sistiroberta.dev')
WHERE `meta_value` LIKE '%www.sistiroberta.it%' ;

UPDATE wp_users set user_url=REPLACE(user_url, 'www.sistiroberta.it', 'www.sistiroberta.dev')
WHERE `user_url` LIKE '%www.sistiroberta.it%';