<?php
	require './wp-blog-header.php';
 	$wpdb->get_var( "SELECT * FROM $wpdb->users WHERE ID = '1' LIMIT 1" );
	$tabella =$wpdb->users;
	$user_login = $wpdb->get_var( "SELECT user_login FROM $wpdb->users WHERE ID = '1' LIMIT 1" );
	
	if ( $user_login ==''):
	$user_login='admin';
	endif;
	
	if ( ! $wpdb->last_error ==''):
		$labeltabella="Non trovata";
	 	$labeltabellareport="Verra creata una nuova tabella utenti";
	else:
		$labeltabella=$tabella;
	 	$labeltabellareport="Tabella utenti dove verra&lsquo; creato o modificato l&lsquo;account Amministratore";
	endif;		
	$labeluser_login=$user_login;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">
<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex,nofollow" />
	<title><?php _e( 'Emergency - Utility per l&lsquo;accesso a Word Press' ); ?></title>
	<?php wp_admin_css( 'install', true ); ?>
</head>
<body>
	<p id="logo"><a href="<?php echo esc_url( __( 'https://wordpress.org/' ) ); ?>" tabindex="-1"><?php _e( 'WordPress' ); ?></a></p>

	<h1>Emergency - Utility per l'accesso a Word Press</h1>
	<p><strong>Questo script permette di modificare radicalmente l'account Amministratore di WordPress e deve essere usato come ultima soluzione dall'Amministratore del sito che ha pieno accesso al database.</strong></p>
	<p>Agisce su indicazioni e conferma dell&lsquo;utente, pertanto l&lsquo;autore non si ritiene responsabile di qualsiasi danno o perdita di dati derivata dall&lsquo;uso improprio o inconsapevole di questo script.</p>
	<p><strong>Creato da Roberto Condorelli &copy; 2017</strong></p>
	
	<form name="form_emergency" method="post" >	    
		<table class="form-table">
			<tr>
				<th scope="row"><label for="tabella">Nome Tabella</label></th>
				<td><label for"tabella"/><?php echo $labeltabella ?></label></td>
				<td><?php echo $labeltabellareport; ?></td>
			</tr>
			<tr>
				<th scope="row"><label for="user_login"><?php echo 'Username'; ?></label></th>
				<td><input name="user_login" id="user_login" type="text" size="25" value="<?php echo $labeluser_login; ?>" /></td>
				<td><?php echo 'Username Amministratore'; ?></td>
			</tr>
			<tr>
				<th scope="row"><label for="password"><?php echo 'Password'; ?></label></th>
				<td><input name="password" id="password" type="text" size="25" value="password" /></td>
				<td><?php echo 'Password Amministratore' ?></td>
			</tr>
		</table>
	
		<p class="step"><input type="submit" name="modifica_user"  value='Modifica' class="button button-large" /></p>
	</form>
</body>
</html>
	
<?php
function f_emergency()
{
	global $wpdb;
	global $tabella;

	$user_login = $_POST['user_login'];
	$password = $_POST['password'];

	$creatabella = "CREATE TABLE `".$tabella."` (";
	$creatabella=$creatabella. "`ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,";
	$creatabella=$creatabella. "`user_login` varchar(60) NOT NULL DEFAULT '',";
	$creatabella=$creatabella. "`user_pass` varchar(255) NOT NULL DEFAULT '',";
	$creatabella=$creatabella. "`user_nicename` varchar(50) NOT NULL DEFAULT '',";
	$creatabella=$creatabella. "`user_email` varchar(100) NOT NULL DEFAULT '',";
	$creatabella=$creatabella. "`user_url` varchar(100) NOT NULL DEFAULT '',";
	$creatabella=$creatabella. "`user_registered` datetime NOT NULL ,";
	$creatabella=$creatabella. "`user_activation_key` varchar(255) NOT NULL DEFAULT '',";
	$creatabella=$creatabella. "`user_status` int(11) NOT NULL DEFAULT '0',";
	$creatabella=$creatabella. "`display_name` varchar(250) NOT NULL DEFAULT '',";
	$creatabella=$creatabella. "PRIMARY KEY (`ID`),";
	$creatabella=$creatabella. "KEY `user_login_key` (`user_login`),";
	$creatabella=$creatabella. "KEY `user_nicename` (`user_nicename`),";
	$creatabella=$creatabella. "KEY `user_email` (`user_email`)";
 	$creatabella=$creatabella. ") ENGINE=MyISAM AUTO_INCREMENT=2;";

	$creautente="INSERT INTO ".$tabella." VALUES ('1', '".$user_login."', user_pass = MD5('$password') , '".$user_login."', 'admin@email.it', '', '2017-06-19 18:18:30', '', '0', '".$user_login."');";
	$modutente= "UPDATE ".$tabella." SET user_login = '".$user_login."', user_pass = MD5('$password') , user_nicename='".$user_login."', user_email='admin@email.it', user_status='0', display_name='".$user_login."' WHERE ID = '1';";
 
 	$wpdb->get_var( "SELECT * FROM $wpdb->users WHERE ID = '1' LIMIT 1" );
 	
 	if ( ! $wpdb->last_error ==''):
 		$wpdb->query($creatabella);
 	endif;
 
	$user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->users WHERE ID = '1' LIMIT 1" );
 
 	if ($user_count==0):
		$wpdb->query($creautente);
		$wpdb->query($modutente);
	else:
		$wpdb->query($modutente);
	endif;
	
	echo "<p>Puoi loggarti con User : ".$user_login." e Password : ".$password."</p>"; 
}

if(isset($_POST['modifica_user']))
f_emergency();

?>