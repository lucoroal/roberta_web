<?php
	/**
	 * Plugin Name: Ricette Plugin

	 * Description: This is a plugin to create <strong>Custom Post Type of Ricette</strong>.
	 * Author: Luigi Sisti
	 *
	 * Version: 1
	 */
	//plugin translation
	function portfolio_plugin_setup() {
		load_plugin_textdomain('portfolio-plugin', false, dirname(plugin_basename(__FILE__)) . '/lang/');
	} // end custom_theme_setup
	add_action('after_setup_theme', 'portfolio_plugin_setup');

	/* ------------------------------------------------------------------------- *
*   CUSTOM POST TYPE Ricette
/* ------------------------------------------------------------------------- */
	add_action('init', 'create_ricette');
	function create_ricette() {
		$labels = array(
			'name'               => __('Ricette' , 'ricetta-plugin'),
			'singular_name'      => __('Ricetta' , 'ricetta-plugin'),
			'add_new'            => __('Aggiungi Ricetta', 'ricetta-plugin'),
			'add_new_item'       => __('Aggiungi nuova Ricetta' , 'ricetta-plugin'),
			'edit_item'          => __('Edit Ricetta', 'ricetta-plugin'),
			'new_item'           => __('Nuova Ricetta', 'ricetta-plugin'),
			'all_items'          => __('Tutte le Ricette', 'ricetta-plugin'),
			'view_item'          => __('Vedi Ricetta' , 'ricetta-plugin'),
			'search_items'       => __('Ricerca Ricetta' , 'ricetta-plugin'),
			'not_found'          => __('Ricetta non trovata', 'ricetta-plugin'),
			'not_found_in_trash' => __('Ricetta non trovata nel cestino', 'ricetta-plugin'),
		);
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'rewrite'            => array('slug' => 'ricetta'),
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => 22,
			'menu_icon'          => 'dashicons-carrot',
			'supports'           => array(
				'title',
				'thumbnail',
				'excerpt',
				'page-attributes'
			),
		);
		register_post_type('ricetta', $args);
	}

	/**
	 * Registers the Custom Post Type hook.
	 * @since 1.0.0
	 * @uses add_action()
	 *
	 * Custom Post Type per inserimento webinar.
	 *                   Campi:
	 *                   Cappello
	 *                   Titolo
	 *                   sottotitolo
	 *                   Descrizione
	 *                   Immagine
	 *                   Data
	 *                   Costo
	 *                   Note
	 *
	 */

	add_action( 'init' ,'wp_new_webinar');

	/**
	 * Creates a new custom post type()
	 * @since 1.0.0
	 * @uses register_post_type()
	 */

	function wp_new_webinar() {

		$labels = array(
			'name'               => __( 'Webinar', 'post type general name', 'ricetta-plugin' ),
			'singular_name'      => __( 'Webinar', 'post type singular name', 'ricetta-plugin' ),
			'menu_name'          => __( 'Webinar', 'admin menu', 'ricetta-plugin' ),
			'name_admin_bar'     => __( 'Webinar', 'add new on admin bar', 'ricetta-plugin' ),
			'add_new'            => __( 'Add New', 'Custom Post Type', 'ricetta-plugin' ),
			'add_new_item'       => __( 'Add New Webinar', 'ricetta-plugin' ),
			'new_item'           => __( 'New Webinar', 'ricetta-plugin' ),
			'edit_item'          => __( 'Edit Webinar', 'ricetta-plugin' ),
			'view_item'          => __( 'View Webinar', 'ricetta-plugin' ),
			'all_items'          => __( 'All Webinar', 'ricetta-plugin' ),
			'search_items'       => __( 'Search Webinar', 'ricetta-plugin' ),
			'parent_item_colon'  => __( 'Parent Webinar:', 'ricetta-plugin' ),
			'not_found'          => __( 'No Webinar found.', 'ricetta-plugin' ),
			'not_found_in_trash' => __( 'No Webinar found in Trash.', 'ricetta-plugin' )
		);

		$args = array(
			'labels'               => $labels, // An array of labels for this post type.
	        'description'          => __( 'Description.', 'ricetta-plugin' ), // A short descriptive summary of what the post type is.
			'public'               => true,    // Whether a post type is intended for use publicly either via the admin interface or by front-end users.
			'publicly_queryable'   => true,    // Whether queries can be performed on the front end for the post type as part of parse_request().
			'show_ui'              => true,    // Whether to generate and allow a UI for managing this post type in the admin.
			'show_in_menu'         => true,    // Whether to show this post type in admin menu.
			'show_in_admin_bar'    => true,    // Whether to make this post available via adminn bar.
			'query_var'            => true,    // Triggers the handling of rewrites for this post type.
			'rewrite'              => array('slug' => 'webinar'),	   // Sets the query_var key for this post type.
			'capability_type'      => 'post',  // The string to use to build the read, edit, and delete capabilities.
			'has_archive'          => true,    // Whether there should be post type archives, or if a string, the archive slug to use.
			'hierarchical'         => false,   // Whether the post type is hierarchical (e.g. page).
			'menu_position'        => 23,    // The position in the menu order the post type should appear. default is null, means at the bottom.
			'menu_icon'          => 'dashicons-desktop',
			'exclude_from_search'  => false,   // Whether to exclude posts in this post type from fron-end search.
			'supports'             => array( 'title', 'editor',  'thumbnail', 'excerpt' ), // Core feature(s) the post type supports.
			'can_export'           => true,    // Whether to allow this post type to be exported.
			'delete_with_user'     => null,    // Whether to delete posts of this type when deleting a user. If true, posts of this type belonging to the user will be moved to trash when then user is deleted. If false, posts of this type belonging to the user will *not* be trashed or deleted.
		);

		register_post_type( 'Webinar', $args ); // Registers the post type.
	}