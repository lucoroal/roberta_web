<?php

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$wp_query_args = array(
	'post_type'   => $_GET['post_type'],
	'search_title'     => $_GET['s'],
	'posts_per_page'=>8,
	'paged'=>$paged,
	'post_status' => 'published',
	'orderby'     => 'date',
	'order'       => 'DESC',
);
$wp_query      = new WP_Query( $wp_query_args );
get_template_part( 'template-parts/content', 'ricette' );