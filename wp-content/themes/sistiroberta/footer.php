</main>

<?php do_action('tailpress_content_end'); ?>

</div>

<?php do_action('tailpress_content_after'); ?>

<footer id="colophon" class="site-footer  pt-12" role="contentinfo">
    <?php do_action('tailpress_footer'); ?>

    <div class=" mx-auto text-center text-verde-scuro font-testoNotizie text-xs  opacity-100 shadow-lg flex flex-col md:flex-row justify-center items-center md:justify-around w-full">
        <div class="bg-white">
            <img src="https://api.thegreenwebfoundation.org/greencheckimage/www.sistiroberta.it?nocache=true"
                 alt="This website is hosted Green - checked by thegreenwebfoundation.org">
        </div>
        <div class="py-3.5 text-verde-scuro font-bold">
            &copy; <?php echo date_i18n('Y'); ?> - <?php echo get_bloginfo('name'); ?>

            <div class="text-verde-scuro font-bold ">Iscrizione Ordine di Biologi sezione A n. AA_069508</div>
            <div class="text-verde-scuro font-bold"> Codice Fiscale: SSTRRT88B66C933N</div>
            <div class="text-verde-scuro font-bold"> Partita I.V.A.: 11870801005</div>
            <div class="text-verde-scuro font-bold"><a href="/contatti"> robertasisti262@gmail.com</a></div>
        </div>
        <div>

            <div>
                <button class="art-button font-extrabold mb-2"><a href="/cookie-policy-ue"> Cookie Policy</a></button>
            </div>
            <div>
                <button class="art-button font-extrabold mb-2"><a href="/privacy"> Privacy</a></button>
                <!--<button class="art-button font-extrabold" value="Privacy Policy"><a
                            href="https://www.iubenda.com/privacy-policy/27325536" class="iubenda-green iubenda-embed "
                            title="Privacy Policy">Privacy Policy</a></button>
                <script type="text/javascript">(function (w, d) {
                        var loader = function () {
                            var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0];
                            s.src = "https://cdn.iubenda.com/iubenda.js";
                            tag.parentNode.insertBefore(s, tag);
                        };
                        if (w.addEventListener) {
                            w.addEventListener("load", loader, false);
                        } else if (w.attachEvent) {
                            w.attachEvent("onload", loader);
                        } else {
                            w.onload = loader;
                        }
                    })(window, document);</script>-->
            </div>
        </div>
    </div>
</footer>

<?php
echo do_shortcode("[topbutton]");
wp_footer(); ?>

</body>
</html>