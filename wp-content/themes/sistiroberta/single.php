<?php get_header();
?>

	<div class="container my-8 mx-auto">

	<?php if ( have_posts() ) : ?>

		<?php

		while ( have_posts() ) :
			the_post();



			?>

			<?php
      $categorie=array();
            foreach (get_the_category() as $index => $item) {

              $categorie[]=strtolower($item->name);
        }




            if ($post->post_type=='ricetta') get_template_part( 'template-parts/content', 'singleRicetta' );
			elseif (in_array('notizie', $categorie)) get_template_part( 'template-parts/content', 'singleNotizia' );
            else get_template_part( 'template-parts/content', 'single' );

            ?>

			<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>

		<?php endwhile; ?>

	<?php endif; ?>

	</div>

<?php
get_footer();