<?php get_header(); ?>

    <div class="container mx-auto my-8">
        <div class="md:flex ">
            <div class="w-full md:w-1/2 flex items-center justify-center">
                <div class="max-w-sm m-8">
                    <div class="text-5xl md:text-15xl text-gray-800 border-primary border-b">404</div>
                    <div class="w-16 h-1 bg-purple-light my-3 md:my-6"></div>
                    <!--  <p class="text-gray-800 text-2xl md:text-3xl font-light mb-8"><?php /*_e( 'Sorry, the page you are looking for could not be found.', 'tailpress' ); */ ?></p>-->
                    <p class="text-green-800 text-2xl md:text-3xl font-light mb-8"><?php echo "Mi dispiace, ma la pagina che cercavi non esiste" ?></p>
                    <a href="<?php echo get_bloginfo('url'); ?>" class="art-button">
                        <?php _e('Go Home', 'tailpress'); ?>
                    </a>
                </div>
            </div>
        </div>

    </div>

<?php
get_footer();