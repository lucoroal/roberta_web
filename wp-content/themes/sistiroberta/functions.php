<?php
    session_start();
    /**
     * Theme setup.
     */
    load_theme_textdomain('sistiroberta', TEMPLATEPATH.'/languages');
    define('THEME_NS', 'sistiroberta');
//__('Read', THEME_NS)
    function tailpress_setup() {
        add_theme_support('title-tag');
        
        register_nav_menus(
            array(
                'primary' => __('Primary Menu', 'tailpress'),
            )
        );
        
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );
        
        add_theme_support('custom-logo');
        add_theme_support('post-thumbnails');
        
        add_theme_support('align-wide');
        add_theme_support('wp-block-styles');
        
        add_theme_support('editor-styles');
        add_editor_style('css/editor-style.css');
    }
    
    add_action('after_setup_theme', 'tailpress_setup');
    
    /**
     * Enqueue theme assets.
     */
    function tailpress_enqueue_scripts() {
        $theme = wp_get_theme();
        
        wp_enqueue_style('tailpress', tailpress_asset('css/app.css'), array(), $theme->get('Version'));
        wp_enqueue_script('tailpress', tailpress_asset('js/app.js'), array(), $theme->get('Version'), 'true');
        wp_enqueue_script('alpinejs', 'https://cdn.jsdelivr.net/npm/alpinejs@3.12.0/dist/cdn.min.js', '', '', 'true');
        wp_enqueue_script('gsap', "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.12.2/gsap.min.js", array(), false,
            true);
        /*wp_register_style('animation', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css');
        wp_enqueue_style('animation');*/
    }
    
    add_action('wp_enqueue_scripts', 'tailpress_enqueue_scripts');
    
    /**
     * Get asset path.
     *
     * @param  string  $path  Path to asset.
     *
     * @return string
     */
    function tailpress_asset($path) {
        if (wp_get_environment_type() === 'production') {
            return get_stylesheet_directory_uri().'/'.$path;
        }
        
        return add_query_arg('time', time(), get_stylesheet_directory_uri().'/'.$path);
    }
    
    /**
     * Adds option 'li_class' to 'wp_nav_menu'.
     *
     * @param  string  $classes  String of classes.
     * @param  mixed  $item  The current item.
     * @param  WP_Term  $args  Holds the nav menu arguments.
     *
     * @return array
     */
    function tailpress_nav_menu_add_li_class($classes, $item, $args, $depth) {
        if (isset($args->li_class)) {
            $classes[] = $args->li_class;
        }
        
        if (isset($args->{"li_class_$depth"})) {
            $classes[] = $args->{"li_class_$depth"};
        }
        
        return $classes;
    }
    
    add_filter('nav_menu_css_class', 'tailpress_nav_menu_add_li_class', 10, 4);
    
    /**
     * Adds option 'submenu_class' to 'wp_nav_menu'.
     *
     * @param  string  $classes  String of classes.
     * @param  mixed  $item  The current item.
     * @param  WP_Term  $args  Holds the nav menu arguments.
     *
     * @return array
     */
    function tailpress_nav_menu_add_submenu_class($classes, $args, $depth) {
        if (isset($args->submenu_class)) {
            $classes[] = $args->submenu_class;
        }
        
        if (isset($args->{"submenu_class_$depth"})) {
            $classes[] = $args->{"submenu_class_$depth"};
        }
        
        return $classes;
    }
    
    add_filter('nav_menu_submenu_css_class', 'tailpress_nav_menu_add_submenu_class', 10, 3);
    
    function add_taxonomies_to_pages() {
        register_taxonomy_for_object_type('post_tag', 'page');
        register_taxonomy_for_object_type('category', 'page');
    }
    
    add_action('init', 'add_taxonomies_to_pages');
    
    add_action('wp_enqueue_scripts', 'sistiroberta_theme_styles_inclusion');
    function sistiroberta_theme_styles_inclusion() {
        wp_register_style('normalize', get_stylesheet_directory_uri().'/css/normalize.css');
        //https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css
        wp_enqueue_style('normalize');
        
        //https://cdn.tailwindcss.com
        
        wp_register_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css');
        wp_enqueue_style('fontawesome');
        
        
        wp_enqueue_script('alpinejs-css', 'https://cdn.tailwindcss.com', array('jquery'), '', true);
        wp_register_style('animation', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css');
        wp_enqueue_style('animation');
        
        
    }
    
    add_filter('clean_url', 'defer_parsing_of_js', 11, 1);
    function defer_parsing_of_js($url) {
        
        if (strpos($url, 'cdn.min.js')) {
            return "$url' defer ";
        } else {
            return $url;
        }
        /*echo $url."<br>";
        if ( FALSE === strpos( $url, '.js' ) ) return $url;
        if ( strpos( $url, 'jquery.js' ) ) return $url;
        else return "$url' defer ";*/
    }
    
    add_shortcode('topbutton', 'topButton');
    
    function topButton(): string {
        
        
        $html = '<button x-data="topBtn" @click="scrolltoTop" id="topButton"
        class="fixed z-10 hidden p-3 bg-gray-100 rounded-full shadow-md bottom-10 left-10 animate-bounce">
        <svg class="w-6 h-6" fill="none" stroke="#51811D" viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2.5" d="M5 10l7-7m0 0l7 7m-7-7v18">
            </path>
        </svg>
    </button>
    ';
        return $html."<script>
       

       
    </script>";
    
    }
    
    add_shortcode('riquadri_home', 'riquadri_home');
    function riquadri_home() {
        
        $arrayRiquadri = array(
            0 => array(
                'titolo' => 'ricette',
                'href' => '/ricette/',
                'src' => "/wp-content/uploads/2017/09/food06-4.jpg",
                'testo' => 'Vuoi un&#8217;idea di qualche ricetta che coniuga il sapore con la corretta alimentazione?',
                'colore' => '#2f89b6'
            ),
            1 => array(
                'titolo' => 'linee guida',
                'href' => '/linee-guida',
                'src' => '/wp-content/uploads/2017/09/linee-guida.jpg',
                'testo' => 'Vuoi alcune regole semplici da seguire per migliorare il tuo stile nel mangiare?',
                'colore' => '#a04661'
            ),
            
            2 => array(
                'titolo' => 'vuoi seguire un percorso di educazione alimentare?',
                'href' => '/calcolo',
                'src' => '/wp-content/uploads/2017/09/dieta-sana-y-variada-para-adelgazar1-150x150.jpg',
                'testo' => 'Hai problemi di peso? Controlla con un semplice test la tua massa corporea (IMC).',
                'colore' => '#5b9121'
            ),
            3 => array(
                'titolo' => 'perchè un nutrizionista',
                'href' => '/perche-un-nutrizionista',
                'src' => '/wp-content/uploads/2017/09/nutrizionista-150x150.jpg',
                'testo' => 'Cosa devi sapere prima di rivolgerti ad uno di noi e cosa ti devi aspettare.',
                'colore' => '#eb9500'
            )
        
        
        );
        $html = "<div class='flex flex-col sm:flex-row  justify-around gap-x-2 ' >";
        //animate__animated animate__fadeInLeft animate__delay-'.$delay.'s
        //<figure><img src="'.get_template_directory_uri().'/assets/image/photo-1606107557195-0e29a4b5b4aa.jpg" alt="Shoes" /></figure>
        $delay = 1;
        foreach ($arrayRiquadri as $index => $item) {
            $html .= '<div class=" bg-['.$item['colore'].']  rounded shadow-lg flex flex-col justify-around hover:shadow-2xl transition duration-200 ease-linear hover:-translate-y-2 hover:shadow-black md:w-1/4 animate__animated animate__fadeInLeft animate__delay-'.$delay.'s" >
					<div class=" font-testoNotizie font-bold text-white uppercase text-center text-[12px]">'.$item['titolo'].'</div>
					<div class="">
						<a href="'.$item['href'].'">
						<img class="mx-auto h-40 mt-2 rounded-3xl transition-shadow hover:shadow-2xl hover:shadow-black border bg-white p-1 " 
                                            src="'.$item['src'].'"
                                            />
           				 </a>
           			</div>
           			<div class="text-verde-scuro rounded text-justify m-1 px-1 leading-4 font-testoDongle text-xl bg-white"><p class="mt-0.5">'.$item['testo'].'</p>
           			<p class="float-right mt-1.5 mb-0.5"><button class="art-button"><a href="'.$item['href'].'"> Leggi&#8230;</a></button></p>
           			</div>
				  	
				  
				</div>';
            
            $delay++;
        }
        
        
        $html .= "</div>";
        return $html;
        
    }
    
    /*
     * Navigatore
     */
//-------------------------------------------------------------------------------------------------------
    function theme_get_option($name) {
        global $theme_default_options;
        $result = get_option($name);
        if ($result === false) {
            $result = theme_get_array_value($theme_default_options, $name);
        }
        return $result;
    }
    
    function theme_page_navigation($slug) {
        global $wp_query;
        
        
        $total_pages = $wp_query->max_num_pages;
        if ($total_pages > 1) {
            echo theme_stylize_pagination($total_pages, max(1, get_query_var('paged')), $slug);
        }
        /*if($total_pages > 1) {
            echo theme_stylize_pagination(paginate_links(array(
                'base'  =>  str_replace(PHP_INT_MAX, '%#%', get_pagenum_link(PHP_INT_MAX)),
                'format' => '',
                'current'   =>  max(1, get_query_var('paged')),
                'total'	 =>  $total_pages
            )));
        }*/
    }
    
    function pianoweb_ripristina_vecchi_pulsanti($row_buttons) {
        array_splice($row_buttons, array_search('italic', $row_buttons) + 1, 0, 'underline');
        array_splice($row_buttons, array_search('alignright', $row_buttons) + 1, 0, 'alignjustify');
        return $row_buttons;
    }
    
    add_filter('mce_buttons', 'pianoweb_ripristina_vecchi_pulsanti', 1);
    
    
    function theme_stylize_pagination($numpage, $attuale, $current_slug) {
        if ($numpage) {
            $pagination = '<div class="bg-white p-4 flex items-center flex-wrap">
  		<nav aria-label="Page navigation" class="mx-auto shadow-lg">
		<ul class="inline-flex">';
            $pagination .= '<li><a href="/'.$current_slug.'/page/1"><button class="px-4 py-2 text-green-600 transition-colors duration-150 bg-white border border-r-0 border-green-600 rounded-l-lg focus:shadow-outline hover:bg-green-100"><i class="fa-solid fa-forward-step fa-rotate-180"></i></button></a></li>';
            if ($attuale > 1) {
                $pagination .= '<li><a href="/'.$current_slug.'/page/'.($attuale - 1).'"><button class="px-4 py-2 text-green-600 transition-colors duration-150 bg-white border border-r-0 border-green-600  focus:shadow-outline hover:bg-green-100"><i class="fa-solid fa-caret-right fa-rotate-180"></i></button></a></li>';
            } else {
                $pagination .= '<li><button class="px-4 pointer-events-none py-2 text-green-600  bg-white border border-r-0 border-green-600  focus:shadow-outline "><i class="fa-solid fa-caret-right fa-rotate-180"></i></button></li>';
            }
            
            for ($x = 1; $x <= $numpage; $x++) {
                if ($x != $attuale) {
                    $pagination .= '<li><a href="/'.$current_slug.'/page/'.$x.'" class=""><button class="px-4 py-2 text-green-600 transition-colors duration-150 bg-white border border-r-0 border-green-600 focus:shadow-outline hover:bg-green-100"> '.($x).'</button></a></li>';
                } else {
                    $pagination .= '<li><button class="px-4 py-2 text-white transition-colors duration-150 bg-green-600 border border-r-0 border-green-600 focus:shadow-outline ">'.$x.'</button></li>';
                }
            }
            if ($attuale < $numpage) {
                $pagination .= '<li><a href="/'.$current_slug.'/page/'.($attuale + 1).'"><button class="px-4 py-2 text-green-600 transition-colors duration-150 bg-white border border-r-0 border-green-600  focus:shadow-outline hover:bg-green-100"><i class="fa-solid fa-caret-right"></i></button></a></li>';
            } else {
                $pagination .= '<li><button class="px-4 py-2 text-green-600 transition-colors duration-150 bg-white border border-green-600  focus:shadow-outline hover:bg-green-100"><i class="fa-solid fa-caret-right"></i></button></li>';
            }
            $pagination .= '<li><a href="/'.$current_slug.'/page/'.$numpage.'"><button class="px-4 py-2 text-green-600 transition-colors duration-150 bg-white border  border-green-600 rounded-r-lg focus:shadow-outline hover:bg-green-100"><i class="fa-solid fa-forward-step"></i></button></a></li>';
            $pagination .= '</ul>
  </nav>
</div>';
            //$pagination = '<div class="art-pager">' . str_replace(array('current', 'dots'), array('current active', 'dots more'), $pagination) . '</div>';
        }
        return $pagination;
    }

//--------------------------------------------------------------------------------------------
// Ripristinare il pulsante Giustificato e Sottolineato
    
    
    add_shortcode('riquadri_iorestoacasa', 'iorestoacasa');
    function iorestoacasa() {
        return;
        
        $html = '<div  class="art-layout-cell-size1 border-verde-scuro bg-red-600 border-double border-2" ><h4 class="text-white">Volantino Agosto 2023</h4>
 <div class="bg-white rounded">
 <p class="aligncenter mb-5 ml-1 font-testoDongle font-bold text-xl text-center"  >Volantino con l`offerta per il mese di agosto sulla Prima visita con valutazione della composizione corporea</p>';
        
        
        $html .= "<hr >";
        $html .= "<div class='flex flex-col md:flex-row text-center justify-around aligncenter space-y-4 md:space-y-0 py-3 shadow-md'>";
        //$html.="<div><a class='art-button' href='https://www.sistiroberta.it/consulenze-online/'>Consulenze Online&#8230;</a></div>";
        $html .= "<div ><button class='art-button'><a  href='".esc_url(get_permalink(843))."'><span class='pl-1'> Consulenze Online&#8230;</span></a></button></div>";
        $html .= "<div><button class='art-button'><a  href='/wp-content/uploads/2023/08/Volantino-AGOSTO-Roby_modificato_2.pdf' target='_blank'><span class='pl-1'>Leggi&#8230;</span></a></button></div>";
        //$html .= "<div><button class='art-button'><a  href='" . esc_url(get_permalink(856)) . "'><span class='pl-1'>Corsi Online&#8230;</span></a></button></div>";
        //$html.="<div><a class='art-button' href='".esc_url(get_page_permalink_from_name('webinar'))."'><span class='pl-1'>Corsi Online&#8230;</span></a></div>";
        
        $html .= "</div>";
        
        $html .= '</div></div><br>';
        
        return $html;
    }
    
    function excerpt_length($id) {
        //return 20; //Di default WP usa 55 parole, in questo caso 20
        
        /*$stringa=substr( $string, 0,110);
        $resto=substr($string, 110);
        $pos=strpos($resto," ");
        $stringa.=substr($resto, 0,$pos-1)." ...";*/
        
        return "<div class='m-1.5 p-1.5 ' >".get_the_excerpt($id).'</div><div class="text-center mb-4"><button class="art-button"><a  href="'.get_permalink($id).'"> Leggi ...</a></button></div>';
        
    }
    
    add_shortcode('ultimiPost', 'ultimiPost');
    function ultimiPost() {
        $args = array('numberposts' => '2', 'post_status' => 'publish', 'exclude' => array(45));
        $recent_posts = wp_get_recent_posts($args);
        
        $post = "<ul class=''>";
        foreach ($recent_posts as $recent) {
            
            
            $post .= '<div class="border rounded mb-2" ><li class=" text-sm  pl-1">
				<span class="font-bold text-xs mx-1"> '.date('d/m/Y',
                    strtotime($recent['post_date']))." </span> ".get_the_post_thumbnail($recent["ID"],
                    "thumb-category").
                '<a class="text-verde underline" href="'.get_permalink($recent["ID"]).'" title="Dettaglio '.esc_attr($recent["post_title"]).'" >'.$recent["post_title"].'</a>
				
               
                <div>'.excerpt_length($recent['ID']).'</div>
               
</li> </div>';
        }
        $post .= "</ul>";
        return $post;
        
    }

//add_filter( 'get_search_form', 'my_search_form' );
    
    function my_search($atts, $content = null) {
        extract(shortcode_atts(array(
            'type' => 'info'
        ), $atts));
        
        
        $form = '
	
	<form role="search" method="get" id="searchform" action="'.home_url('/').'" >
		<label class="screen-reader-text" for="s">Search query:</label>
		<input type="text" class="border rounded" value="'.get_search_query().'" name="s" id="s" />
		
		<button type="submit" id="searchsubmit" class="art-button"><i class="fa-solid fa-magnifying-glass mr-1"></i>Cerca..</button>
		
		<input type="hidden" value="'.$atts['post'].'" name="post_type" />
	</form>
	';
        
        
        return $form;
    }
    
    add_shortcode('search', 'my_search');
    /*
     * Filtro per cercare sul campo title del post
     */
    add_filter('posts_where', 'lsisti_posts_where', 10, 2);
    function lsisti_posts_where($where, $wp_query) {
        
        
        global $wpdb;
        if ($title = $wp_query->get('search_title')) {
            $where .= " AND ".$wpdb->posts.".post_title LIKE '%".esc_sql($wpdb->esc_like($title))."%'";
        }
        return $where;
    }
    
    /**
     * Shortcode per calcolare la massa
     */
    add_shortcode('calcoloMassa', 'calcoloMassa');
    function calcoloMassa() {
        $campi = '<form class="mx-auto shadow-lg rounded-lg" method="get" >';
        $campi .= '<div class="space-y-12">';
        $campi .= '<div class="border-b border-gray-900/10 pb-12 mx-auto mb-2">';
        
        $campi .= '<div class="mt-10 grid grid-cols-3 gap-x-6 gap-y-8 sm:grid-cols-12 place-content-center ">';
        $campi .= '<div class="sm:col-span-2"></div>';
        $campi .= '<div class="sm:col-span-2"></div>';
        $campi .= '<div class="sm:col-span-4">
          <label for="sesso" class="block text-sm font-medium leading-6 text-gray-900">Sesso <span class="text-red-600 italic">*</span></label>
          <div class="mt-2">
          <span class="text-red-800 hidden shadow-md p-2 mb-3 italic text-xs" id="chksesso"> </span>
            <select id="sesso" name="sesso"  class=" parametri block w-full rounded-md border-0 my-3 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6">
              <option value="">-- Scegli --</option>
              <option value="M">Maschio</option>
              <option value="D">Donna</option>
             
            </select>
                      </div>
          <div class="sm:col-span-3">
          <label for="Età" class="block text-sm font-medium leading-6 text-gray-900 ">Et&agrave; <span class="text-red-600 ">*</span></label>
          <div class="mt-2">
          <span class="text-red-800 hidden shadow-md p-2 m-3 italic text-xs" id="chketa"></span>
            <input type="number"  min="5" max="100" step="1" required name="eta" id="eta"  class=" parametri my-3 block pl-1.5 w-15 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
            </div>
        </div>
        <div class="sm:col-span-3">
          <label for="altezza" class="block text-sm font-medium leading-6 text-gray-900 italic">Altezza (in centimetri) <span class="text-red-600 ">*</span></label>
          <div class="mt-2">
          <span class="text-red-800 hidden shadow-md p-2 m-3 italic text-xs" id="chkaltezza"></span>
            <input type="number" min="50"  max="210" step="1" required name="altezza" id="altezza"  class="parametri my-3 pl-1.5 block w-15 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
            
          </div>
        </div>
           <div class="sm:col-span-3">
          <label for="peso" class="block text-sm font-medium leading-6 text-gray-900 ">Peso (in kg) <span class="text-red-600 ">*</span> </label>
          <div class="mt-2">
          <span class="text-red-800 hidden shadow-md p-2 m-3 italic text-xs" id="chkpeso"></span>
            <input type="number" max="200" min="20" step="1" required name="peso" id="peso"  class="parametri my-3 pl-1.5 block w-15 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
         
          </div>
        </div>
        </div>';
        
        $campi .= '</div>';
        
        $campi .= '</div>';
        $campi .= '</div>
<div class="text-center mx-auto w-full">Massa grassa: <span id="percentuale" style="font-weight: bold; font-size: 12px"></span>  </div>
<div class="mt-6 flex items-center justify-center gap-x-6">
    <button type="button" class="art-button pb-2 mb-4" onclick="Calcola()">Calcola</button>
    
  </div>';
        $campi .= '</form>';
        $campi .= "<script type='text/javascript'>
    function arrotonda(valore, nCifre){
          if(isNaN(parseFloat(valore)) || isNaN(parseInt(nCifre)) )
            return false;
          else
            return Math.round(valore * Math.pow(10,nCifre)) / Math.pow(10,nCifre);
        }
        
        function checkCampi(){
        const collection = document.getElementsByClassName('parametri');
      
        for (x=0;x<collection.length;x++) {
         
            if (document.getElementById(collection[x].id).value==''){
                 document.getElementById('chk' + collection[x].id).textContent='Campo richiesto';
                document.getElementById('chk'+collection[x].id).classList.remove('hidden');
              
                return false;
            }
            else {
                document.getElementById('chk'+collection[x].id).classList.add('hidden');
                document.getElementById('chk'+collection[x].id).innerHtml='';
            }
        }
         
            return true
        }
        function Calcola(){
            if (checkCampi()==true){
                   /*(1,20 x IMC) + (0,23 x età) - (10,8 x sesso) - 5,4
            * 1 per maschi 0 per donne
            * */
            
           var sesso= (document.getElementById('sesso').value=='M'?1:0);
           var statura= document.getElementById('altezza').value;
           var peso= document.getElementById('peso').value;
           var eta= document.getElementById('eta').value;
           
           var IMC=(peso / Math.pow(statura/100,2));
          
           
         
               var percentuale=(1.20*IMC)+(0.23*eta)-(10.8*sesso)-5.4;
              
          
            document.getElementById('percentuale').innerHTML=arrotonda(percentuale,2) + '%';
            } 
            
        
        
         
        }
    </script>";
        return $campi;
    }
    
    
    add_shortcode('riquadri_webinar', 'mywebinar');
    function mywebinar() {
        
        $imgGosrc = "<img src='".get_template_directory_uri()."/images/icons8-freccia-26.png' style='border:none!important'>";
        $imgData = "<img src='".get_template_directory_uri()."/images/icons8-calendario-24.png' >";
        $imgCosto = "<img src='".get_template_directory_uri()."/images/icons8-monete-48.png' >";
        
        $args = array('post_status' => 'publish', 'post_type' => 'webinar', 'numberposts' => 4);
        $recent_posts = wp_get_recent_posts($args);
        $html = "<div class='text-center '> <h2 class='text-2xl'>I miei ultimi webinar</h2></div>";
        $html .= "<div class='flex flex-col md:flex-row justify-around '>";
        foreach ($recent_posts as $recent) {
            
            $valori = get_fields($recent['ID']);
            
            
            $image = get_field('locandina', $recent['ID']);
            
            $html .= "<div class='shadow-lg text-center m-2 p-2 w-5/12 rounded-2xl flex flex-col justify-between' >";
            $html .= '<div><h3 style="text-align: center">'.$recent["post_title"].'</h3></div>';
            $html .= '<div>'.$valori['motto'].'</div>';
            $html .= '<div class="mx-auto">'.wp_get_attachment_image($image, 'thumbnail', '',
                    array('class' => 'mx-auto')).'</div>';
            $html .= '<div>'.$valori['sottotitolo'].'</div>';
            $html .= '<div class="flex flex-row justify-around items-center ">';
            $html .= "<div >".$imgData."</div><div><span class='text-sm'>".$valori['data_webinar']."</span></div>";
            $html .= "<div >".$imgCosto."</div><div>€ ".$valori['costo']."</div>";
            
            
            $html .= '</div>';
            $html .= "<div ><a href='".get_permalink($recent['ID'])."' class='float-right'>{$imgGosrc}</a></div>";
            $html .= '<div></div>';
            
            
            $html .= "</div>";
            
            
        }
        $html .= "</div>";
        
        $html .= "<div class='text-center mt-4'><a class='art-button' href='".get_post_type_archive_link('webinar')."'>Vedi tutti ...</a></div>";
        
        
        return $html;
    }
    
    
    /* function add_custom_class($classes = array(), $menu_item = false) {
         
         if (is_single() && ('post' == get_post_type() || 'page' == get_post_type()) && 'News' == $menu_item->title && !in_array('current-menu-item',
                 $classes)) {
             $classes[] = 'current-menu-item'; // setting current menu item
         }
         $classes[] = 'current-menu-item';
         return $classes;
     }
     
     add_filter('nav_menu_css_class', 'add_custom_class', 100, 2);*/
    /**
     * Visualizzazione pagine con maintanance mode
     */
    
    
    if ($_GET['ROBERTASISTI'] || $_SESSION['ROBERTASISTI']) {
        
        
        if (!isset($_SESSION['ROBERTASISTI'])) {
            $_SESSION['ROBERTASISTI'] = true;
        }
        
        remove_action('init', 'csmm_plugin_init');
    }