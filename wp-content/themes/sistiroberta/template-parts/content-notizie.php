<?php
    get_header();
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $wp_query_args = array(
        'cat=29',
        'post_status' => 'publish',
        'posts_per_page' => 8,
        'paged' => $paged,
        'orderby' => 'date',
        'order' => 'DESC',
    );
    
    $wp_query = new WP_Query($wp_query_args);
    $testo = "<div class='flex flex-col sm:flex-row flex-wrap m-2 justify-around'>";
    
    
    foreach ($wp_query->posts as $index => $post) {
        
        //$image = get_field( 'immagine' );
        
        
        //$testo .= "<div class='w-1/5 m-2  p-1.5 flex flex-col '>";
        $testo .= '<div class="max-w-sm rounded overflow-hidden shadow-lg mx-auto px-2  m-2  p-1.5 flex flex-col justify-between hover:shadow-2xl transition duration-200 ease-linear hover:-translate-y-2 hover:shadow-black">';
        //$testo .= wp_get_attachment_image( $image, 'thumbnail' );
        $testo .= ' <div class="px-2 py-1">
			    <div class="font-bold text-xl mb-2 font-testoQuicksand text-verde">'.$post->post_title.'</div>
			    <p class="text-gray-700 text-base font-testoricette">'.$post->post_excerpt.'</p>';
        $testo .= '
			  </div>
			  <div class="px-6 pt-4 pb-2 text-right">
			    <button class="art-button"><a href='.get_permalink($post->ID).'> Leggi&#8230;</a></button>
			  </div>
			</div>';
        //$testo .= "</div>";
        
    }
    
    $testo .= "</div>";
    echo $testo;
    if (theme_get_option('theme_bottom_posts_navigation')) {
        theme_page_navigation('notizie');
    }
    
    get_footer();