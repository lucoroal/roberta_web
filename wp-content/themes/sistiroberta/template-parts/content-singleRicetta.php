<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header mb-4">
		<?php the_title( sprintf( '<h1 class="entry-title text-2xl lg:text-5xl font-extrabold leading-tight mb-1"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

    </header>


	<?php
	//$post=get_post();

	$image = get_field('immagine');
	$custom = [ 'class' => 'rounded-2xl', 'alt' => $post->post_title, 'title' => $post->post_title ];

    $testo="<div class='my-2 font-testoLato'>".get_the_excerpt($post->ID)."</div>";
    $testo.="<div class='flex flex-col md:flex-row justify-around' >";
    $testo.="<div >".wp_get_attachment_image( $image, 'medium','',$custom)."</div>";
    $testo.="<div id='ingredienti' class='flex flex-col justify-center font-testoLato'>".get_field( 'ingredienti')."</div>";

    $testo.="</div>";
    $testo.="<div class='mt-2 font-testoQuicksand'>".get_field( 'preparazione')."</div>";
    $testo.='<div class="px-6 pt-4 pb-2">
			    <button class="art-button"><a href=' . get_permalink(473) . '> Vai elenco ricette&#8230;</a></button>
			  </div>';

    echo $testo;




	?>


</article>