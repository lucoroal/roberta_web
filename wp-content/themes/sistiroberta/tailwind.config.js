const theme = require('./theme.json');
const tailpress = require("@jeffreyvr/tailwindcss-tailpress");

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './*.php',
        './**/*.php',
        './resources/css/*.css',
        './resources/js/*.js',
        './safelist.txt'
    ],
    important: true,
    theme: {
        container: {
            padding: {
                DEFAULT: '1rem',
                sm: '2rem',
                lg: '0rem'
            },
        },
        extend: {
            colors: tailpress.colorMapper(tailpress.theme('settings.color.palette', theme)),
            colors: {
                'verde-scuro': '#365e0c',
                'verde': '#6DAF28',
                'verde-chiaro': '#afcc2e',
                'salmone': '#EB9500',
                'bordeaux': '#A04661',
                'blu-acqua': '#0693e3',
                'rosso': '#cf2e2e',
                'terra':'#4e3b31',
                'testo':'#9c9c9c',
                'sfondo-footer': '#e2fece',

            },
            fontSize: tailpress.fontSizeMapper(tailpress.theme('settings.typography.fontSizes', theme)),
            fontFamily: {
                "testoTitolo": ['Pacifico', 'cursive'],
                "testo": ['Roboto', 'sans-serif'],
                "testoricette":['Quicksand','sans-serif'],
                "testoDongle": ['Dongle', 'sans-serif'],
                "testoLato": ['Lato', 'sans-serif'],
                "testoNotizie":['Montserrat','sans-serif'],
                "testoQuicksand":['Quicksand','sans-serif'],


            },
            listStyleImage: {
                checkmark: "url('../../assets/image/icone/postbullets.png')",
            },
        },
        screens: {
            'xs': '480px',
            'sm': '600px',
            'md': '782px',
            'lg': tailpress.theme('settings.layout.contentSize', theme),
            'xl': tailpress.theme('settings.layout.wideSize', theme),
            '2xl': '1440px'
        },
        backgroundImage: {

            'pattern': "url('../../assets/image/page.jpeg')"

        }
    },
    plugins: [
        tailpress.tailwind
    ]
};