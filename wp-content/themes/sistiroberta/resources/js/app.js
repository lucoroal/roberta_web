// Navigation toggle
window.addEventListener('load', function () {
    let main_navigation = document.querySelector('#primary-menu');
    document.querySelector('#primary-menu-toggle').addEventListener('click', function (e) {
        e.preventDefault();
        main_navigation.classList.toggle('hidden');
    });
    gsap.fromTo("#menu-top-menu li", {opacity: 0, x: 0}, {opacity: 1, ease: "steps(1)", duration: .5, stagger: 0.25});
    //gsap.from('#riquadri',{duration:1.5,opacity:0,scale:0.3,ease:"back"});


});
const topBtn = document.getElementById("topButton");
const topHead = document.getElementById("header");
const topLogo = document.getElementById("TESTOLOGO");
const altezza = 30
window.onscroll = () => {
    (document.body.scrollTop > altezza || document.documentElement.scrollTop > altezza) ? topBtn.classList.remove("hidden") : topBtn.classList.add("hidden");
    (document.body.scrollTop > altezza || document.documentElement.scrollTop > altezza) ? topHead.classList.add("bg-gray-50") : topHead.classList.remove("bg-gray-50");

    if (document.body.scrollTop > altezza || document.documentElement.scrollTop > altezza) {
        topLogo.classList.add("text-verde");
        topLogo.classList.remove("text-white");
    } else {
        topLogo.classList.add("text-white");
        topLogo.classList.remove("text-verde");
    }


}

document.addEventListener('alpine:init', () => {
    Alpine.data('topBtn', () => ({

        scrolltoTop() {
            /*document.body.scrollTop = 0;

            document.documentElement.scrollTop = 0;*/

            window.scroll({top: 0, behavior: 'smooth'});
        }
    }));
});