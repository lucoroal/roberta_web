<?php

/**
 *
 * search.php
 *
 * The search results template. Used when a search is performed.
 *
 */
get_header();
?>
			<?php get_sidebar('top'); ?>
			<?php
			if (have_posts()) {
				theme_post_wrapper(
						array('content' => '<h4 class="box-title">' . sprintf(__('Search Results for: %s', THEME_NS), '<span class="search-query-string">' . get_search_query() . '</span>') . '</h4>'
						)
				);
				/* Display navigation to next/previous pages when applicable */
				if (theme_get_option('theme_top_posts_navigation')) {
					theme_page_navigation();
				}
				/* Start the Loop */
				while (have_posts()) {
					the_post();



					if (get_post_type()=='ricetta'){
						include_once 'search-ricetta.php';
						//get_template_part('content', 'search-ricetta');

					}
					else{

				get_template_part('content', 'search');
						echo "<div align='center'><a class='art-button' href='" . get_the_permalink() . "'>Leggi …</a></div>";

						/*$testo = '';
						$image = get_field( 'immagine' );
						$testo .= "<div style='width: 100%' class='ricetta'>";
						$testo .= "<div style='width: 19%;float: left;margin-top: 20px;'><div align='center'> " . wp_get_attachment_image( $image, 'thumbnail' ) . "</div></div>";
						$testo .= "<div style='width: 75%;float: left'>";
						$testo .= "<div class='titolo'>" . get_the_title() . "</div><hr>";
						$testo .= "<div class='excerpt'>" . get_the_excerpt() . "</div>";
						$testo .= "<div align='center'><a class='art-button' href='" . get_the_permalink() . "'>Leggi …</a></div>";
						$testo .= "</div>";
						$testo .= "</div>";
						$testo .= "<div style='clear: both'></div>";

						echo $testo;*/


					}

				}
				/* Display navigation to next/previous pages when applicable */
				if (theme_get_option('theme_bottom_posts_navigation')) {
					theme_page_navigation();
				}
			} else {
				theme_404_content(
						array(
							'error_title' => __('Nothing Found', THEME_NS),
							'error_message' => __('Sorry, but nothing matched your search criteria. Please try again with some different keywords.', THEME_NS)
						)
				);
			}
			?>
			<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>