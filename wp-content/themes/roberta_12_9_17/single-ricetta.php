<?php

/**
 *
 * single.php
 *
 * The single post template. Used when a single post is queried.
 * 
 */
get_header();
get_sidebar('top');
if(have_posts()){

	$testo="<div class='ricetta'>";
	while (have_posts()) {
		the_post();

		$image = get_field('immagine');
		$testo.="<div class='titolo_big'>".get_the_title()."</div><hr>";
		$testo.="<div class='excerpt'>".get_the_excerpt()."</div>";
		$testo.="<div class='titolo' >Ingredienti</div>";
		$testo.="<div align='center'> ".wp_get_attachment_image( $image, 'medium' )."</div>";
		$testo.="<div class='ingredienti'>".get_field( 'ingredienti')."</div>";
		/*$testo.="<div style='width: 100%'>";
			$testo.="<div style='width: 49%;float: left;margin-top: 20px;'><div align='center'> ".wp_get_attachment_image( $image, 'medium' )."</div></div>";
			$testo.="<div style='width: 49%;float: right'>
				
				<div class='ingredienti'>".get_field( 'ingredienti')."</div>
			</div>";*/


		$testo.="<div style='clear: both'></div>";
		$testo.="<hr><div class='titolo' >Preparazione</div>";
		$testo.= "<div class='preparazione'>".get_field( 'preparazione')."</div>";

	}

	$testo.="</div>";

}
else{
	theme_404_content();
}
echo $testo;
if (shortcode_exists('iconaPdf')) echo do_shortcode("[iconaPdf]");

get_sidebar('bottom');
get_footer();

