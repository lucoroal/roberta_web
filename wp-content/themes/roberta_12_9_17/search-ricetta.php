<?php
	/* Start the Loop */

		$testo = '';
		$testo.="<article id='".get_the_ID()."' class=\"art-post art-article  post-434 page type-page status-publish hentry\">
				<div class=\"art-postheadericons art-metadata-icons\">
					<span class=\"art-postdateicon\">
						<span class=\"date\">Pubblicato</span> 
						<span class=\"entry-date\" title=\"14:31\">". get_the_date()."</span>
					</span>
				</div>";
		$image = get_field( 'immagine' );
		$testo .= "<div style='width: 100%' class='ricetta'>";
		$testo .= "<div style='width: 19%;float: left;margin-top: 20px;'><div align='center'> " . wp_get_attachment_image( $image, 'thumbnail' ) . "</div></div>";
		$testo .= "<div style='width: 75%;float: left'>";
		$testo .= "<div class='titolo'>" . get_the_title() . "</div><hr>";
		$testo .= "<div class='excerpt'>" . get_the_excerpt() . "</div>";
		$testo .= "<div align='center'><a class='art-button' href='" . get_the_permalink() . "'>Leggi …</a></div>";
		$testo .= "</div>";
		$testo .= "</div>";
		$testo .= "<div style='clear: both'></div>";

		echo $testo;

