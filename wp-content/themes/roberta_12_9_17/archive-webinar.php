<?php
/**
 * Created by PhpStorm.
 * User: Luigi Sisti
 * Date: 26/04/2021 16:49
 *
 */
get_header();
get_sidebar('top');
echo do_shortcode('[riquadri_webinar]');

if (theme_get_option('theme_bottom_posts_navigation')) {
	theme_page_navigation();
}


wp_reset_postdata();

get_sidebar('bottom');
get_footer();