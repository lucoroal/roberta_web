<?php /* Template Name: Notizie */
?>
<?php
	/**
	 * Created by PhpStorm.
	 * User: Luigi Sisti
	 * Date: 12/09/2017 14:21
	 *
	 */

	get_header();
	get_sidebar('top');
	//'posts_per_page' => 2,
	$wp_query_args = array(
		'cat=29',
		'posts_per_page' => 3,
		'paged'=>$paged,
		'orderby' => 'date',
		'order' => 'DESC',
	);
	$wp_query = new WP_Query( $wp_query_args );
	$testo="<div class='ricetta'>";
	if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
		$image = get_field('immagine');
		$testo.="<div style='width: 100%'>";

		$testo.="<div class='titolo'>".get_the_title()."</div><span style='font-size: 10px;font-weight: bold;font-style: italic'> (".get_the_date().")</span>";
		$testo.="<div class='excerpt'>".get_the_excerpt()."</div>";
		$testo.="<div align='center'><a class='art-button' href='".get_the_permalink()."'>Leggi …</a></div>";

		$testo.="<div style='clear: both'></div>";
		$testo.="<hr>";
		$testo.="</div>";

	endwhile;
	endif;
	$testo.="</div>";
	echo $testo;
	/* Display navigation to next/previous pages when applicable */
	if (theme_get_option('theme_bottom_posts_navigation')) {
		theme_page_navigation();
	}



	wp_reset_postdata();

	get_sidebar('bottom');
	get_footer();
?>