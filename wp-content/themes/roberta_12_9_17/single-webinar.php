<?php
/**
 * Created by PhpStorm.
 * User: Luigi Sisti
 * Date: 26/04/2021 16:50
 *
 */
get_header();
get_sidebar('top');
if(have_posts()){
	$imgGosrc="<img src='".get_template_directory_uri()."/images/icons8-freccia-26.png' style='border:none!important'>";
	$imgData="<img src='".get_template_directory_uri()."/images/icons8-calendario-24.png' style='border:none!important;width:26px;vertical-align: middle;'>";
	$imgCosto="<img src='".get_template_directory_uri()."/images/icons8-monete-48.png' style='border:none!important;width:26px;vertical-align: middle;'>";
	$html="<div class='ricetta' style='margin-left: 80px!important;
    							margin-right: 80px!important;
   
    font-size: 12px;'>";
	while (have_posts()) {
		the_post();


		$valori=get_fields();
		$html.="<div class='titolo_big'>".get_the_title();
		$html.="<p align='center'> ".$valori['sottotitolo']."</p></div><hr>";
		$html.="<div align='center'> <h3>".$valori['motto']."</h3></div>";
		$html.="<div align='center'> ".wp_get_attachment_image( $valori['locandina'], 'medium' )."</div>";



		$html.="<div style='margin-bottom: 10px;padding-bottom: 10px'> ".get_the_content()."</div>";



		$html.="<p style='font-weight: bold;margin-bottom: 10px;padding-bottom: 10px'><span style='float: left' >{$imgData} <span class='badge'>{$valori['data_webinar']}</span> </span><span style='float: right'>{$imgCosto} <span class='badge'> € {$valori['costo']}</span></span></p><br><br> ";

		$html.="<hr>";
		$html.="<div class='nota' > ".$valori['note']."</div>";
		$html.="<div class='nota'>Per info e iscrizione:<br>
									3280818123<br>
							robertasisti262@gmail.com</div>";

		/*echo "<pre>";
		print_r($valori);*/

	}
	$html.="</div>";
}
else{
	theme_404_content();
}

$html.="<div style='text-align: center'><a class='art-button' href='".get_post_type_archive_link( 'webinar' )."'>Vedi tutti ...</a></div>";
echo $html;


get_sidebar('bottom');
get_footer();