<?php /* Template Name: calcoloMassa */ ?>
<?php
	/**
	 * Created by PhpStorm.
	 * User: Luigi Sisti
	 * Date: 12/09/2017 14:21
	 *
	 */

	get_header();
	get_sidebar('top');
	if (have_posts()) {
		/* Start the Loop */
		while (have_posts()) {
			the_post();
			get_template_part('content', 'page');
		}
	} else {
		theme_404_content();
	}
	get_sidebar('bottom');
	get_footer();
?>