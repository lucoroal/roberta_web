<?php

if (getenv('IS_DDEV_PROJECT') == 'true') {

    /* define('DB_USER', 'db');
     define('DB_PASSWORD', 'db');
     define('DB_TABLE_PREFIX', 'wp_');
     define('DB_HOST', 'ddev-www.sistiroberta-db');

     $table_prefix = 'wp_';
     define('DB_NAME', 'sistiroberta');*/

    defined('DB_NAME') or define('DB_NAME', 'sistiroberta');
    defined('DB_USER') or define('DB_USER', 'db');
    defined('DB_PASSWORD') or define('DB_PASSWORD', 'db');


    /** MySQL hostname */
    defined('DB_HOST') || define('DB_HOST', 'ddev-www.sistiroberta-db');

    /** WP_HOME URL */
    defined('WP_HOME') || define('WP_HOME', 'https://www.sistiroberta.dev');

    /** WP_SITEURL location */
    defined('WP_SITEURL') || define('WP_SITEURL', 'https://www.sistiroberta.dev' . '/');

    /** Enable debug */
    defined('WP_DEBUG') || define('WP_DEBUG', $_ENV['WP_DEBUG']);

    /**
     * Set WordPress Database Table prefix if not already set.
     *
     * @global string $table_prefix
     */
    if (!isset($_ENV['DB_TABLE_PREFIX']) || empty($_ENV['DB_TABLE_PREFIX'])) {
        // phpcs:disable WordPress.WP.GlobalVariablesOverride.Prohibited
        $table_prefix = 'wp_';
        // phpcs:enable

    }
} else {

    $table_prefix = "wp_";
    /** Il nome del database di WordPress */
    define('DB_NAME', 'sistirob33668');

    /** Nome utente del database MySQL */
    define('DB_USER', 'sistirob33668');

    /** Password del database MySQL */
    define('DB_PASSWORD', '170760LuCo$');

    /** Hostname MySQL  */
    define('DB_HOST', 'sql.sistiroberta.it');


    /** Charset del Database da utilizzare nella creazione delle tabelle. */
    define('DB_CHARSET', 'utf8');
    /** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
    define('DB_COLLATE', '');
}