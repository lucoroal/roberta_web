#!/bin/bash

# Set TABLETOFIND to a table which should exist in a loaded database
TABLETOFIND=sistiroberta.wp_options

if ! mysql -e "SELECT * FROM ${TABLETOFIND};" sistiroberta >/dev/null 2>&1; then
  echo "loading database since table named '${TABLETOFIND}' was not found."
  # This assumes the db.sql.gz is in the root of your repository, but
  # adjust as necessary.
  mysql -uroot -proot --one-database sistiroberta < ./database/sql/dump.sql
else
  echo "NOT loading database; it already exists; table '${TABLETOFIND}' was found."
fi